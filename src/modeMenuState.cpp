#include <SFML/Window/Event.hpp>

#include "aspectRatio.hpp"
#include "matchState.hpp"
#include "modeMenuState.hpp"

ModeMenuState::ModeMenuState ( StateStack& stateStack, sf::RenderWindow& window ) :
	stateStack ( stateStack ),
	window ( window )
{
	//Load textures
	backgroundTexture.loadFromFile ( "Sprites/Court.png" );

	//Assign textures to sprites
	backgroundSprite.setTexture ( backgroundTexture );

	//Load font
	menuFont.loadFromFile ( "Fonts/Aileron-BlackItalic.otf" );
	titleFont.loadFromFile ( "Fonts/gooddog-plain.regular.ttf" );

	//Assign font to texts
	titleText.setFont ( titleFont );
	regularText.setFont ( menuFont );
	superText.setFont ( menuFont );
	verySuperText.setFont ( menuFont );
	backText.setFont ( menuFont );

	//Set up texts
	titleText.setString ( "SUPER WORM\n VOLLEYBALL" );
	regularText.setString ( "Regular" );
	superText.setString ( "Super" );
	verySuperText.setString ( "Very Super" );
	backText.setString("Back");

	titleText.setCharacterSize ( 225 );
	regularText.setCharacterSize ( 50 );
	superText.setCharacterSize ( 50 );
	verySuperText.setCharacterSize ( 50 );
	backText.setCharacterSize ( 50 );

	titleText.setFillColor ( sf::Color::Black );
	regularText.setFillColor ( sf::Color::Black );
	superText.setFillColor ( sf::Color::Black );
	verySuperText.setFillColor ( sf::Color::Black );
	backText.setFillColor ( sf::Color::Black );

	regularText.setOutlineColor ( sf::Color::White );
	superText.setOutlineColor ( sf::Color::White );
	verySuperText.setOutlineColor ( sf::Color::White );
	backText.setOutlineColor ( sf::Color::White );

	regularText.setOutlineThickness ( 0 );
	superText.setOutlineThickness ( 0 );
	verySuperText.setOutlineThickness ( 0 );
	backText.setOutlineThickness ( 0 );

	titleText.setPosition ( 960 - titleText.getGlobalBounds().width/2, 0 );
	regularText.setPosition ( 200, 625 );
	superText.setPosition ( 200, 700 );
	verySuperText.setPosition ( 200, 775 );
	backText.setPosition ( 200, 850 );
}

void ModeMenuState::draw()
{
	window.clear();
	window.draw(backgroundSprite);
	window.draw(titleText);
	window.draw(regularText);
	window.draw(superText);
	window.draw(verySuperText);
	window.draw(backText);
	window.display();
}

void ModeMenuState::handleEvents()
{
	sf::Event event;
	while ( window.pollEvent(event) ) {
		switch ( event.type ) {

		//Close the window if the close button has been pressed
		case sf::Event::Closed:
			window.close();
			break;

		//Check whether anything clickable was clicked
		case sf::Event::MouseButtonPressed:
			if ( regularText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.push ( std::make_unique<MatchState> ( stateStack, window ) );
			} else if ( superText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.push ( std::make_unique<MatchState> ( stateStack, window, 0.0075f ) );
			} else if ( verySuperText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.push ( std::make_unique<MatchState> ( stateStack, window, 0.04f ) );
			} else if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.pop ();
			}
			break;

		case sf::Event::Resized:
			SetAspectRatio( window, 16, 9 );
			break;

		default:
			break;
		}
	}
}

void ModeMenuState::update ( double )
{
	if ( regularText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		regularText.setOutlineThickness ( 5 );
	} else {
		regularText.setOutlineThickness ( 0 );
	}

	if ( superText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		superText.setOutlineThickness ( 5 );
	} else {
		superText.setOutlineThickness ( 0 );
	}

	if ( verySuperText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		verySuperText.setOutlineThickness ( 5 );
	} else {
		verySuperText.setOutlineThickness ( 0 );
	}

	if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		backText.setOutlineThickness ( 5 );
	} else {
		backText.setOutlineThickness ( 0 );
	}
}
