#include <atomic>
#include <fstream>
#include <iostream>
#include <memory>
#include <stack>
#include <string>
#include <thread>

#include <SFML/Network/IpAddress.hpp>

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Window/Event.hpp>

#include "aspectRatio.hpp"
#include "connectionMenuState.hpp"
#include "matchState.hpp"
#include "modeMenuState.hpp"

//Construct the state
ConnectionMenuState::ConnectionMenuState ( StateStack& stateStack, sf::RenderWindow& window ) :
	menuState ( MenuState::BaseMenu ),
	stateStack ( stateStack ),
	window ( window ),
	connectedToOpponent ( false )
{
	//Load textures
	backgroundTexture.loadFromFile ( "Sprites/TitleScreen.png" );

	//Assign textures to sprites
	backgroundSprite.setTexture ( backgroundTexture );

	//Load fonts
	menuFont.loadFromFile ( "Fonts/Aileron-BlackItalic.otf" );

	//Assign fonts to texts
	localText.setFont ( menuFont );
	hostText.setFont ( menuFont );
	joinText.setFont ( menuFont );
	backText.setFont ( menuFont );
	ipText.setFont ( menuFont );
	waitingText.setFont ( menuFont );
	connectingText.setFont ( menuFont );

	//Set up texts
	localText.setString ( "Local Game" );
	hostText.setString ( "Host Game" );
	joinText.setString ( "Join Game" );
	backText.setString ( "Back" );
	waitingText.setString ( "Waiting for opponent to connect" );

	localText.setCharacterSize ( 50 );
	hostText.setCharacterSize ( 50 );
	joinText.setCharacterSize ( 50 );
	backText.setCharacterSize ( 50 );
	ipText.setCharacterSize ( 25 );
	waitingText.setCharacterSize ( 25 );
	connectingText.setCharacterSize ( 25 );

	localText.setFillColor ( sf::Color::Black );
	hostText.setFillColor ( sf::Color::Black );
	joinText.setFillColor ( sf::Color::Black );
	backText.setFillColor ( sf::Color::Black );
	ipText.setFillColor ( sf::Color::Black );
	waitingText.setFillColor ( sf::Color::Black );
	connectingText.setFillColor ( sf::Color::Black );

	localText.setOutlineColor ( sf::Color::White );
	hostText.setOutlineColor ( sf::Color::White );
	joinText.setOutlineColor ( sf::Color::White );
	backText.setOutlineColor ( sf::Color::White );
	ipText.setOutlineColor ( sf::Color::White );
	waitingText.setOutlineColor ( sf::Color::White );
	connectingText.setOutlineColor ( sf::Color::White );

	localText.setOutlineThickness ( 0 );
	hostText.setOutlineThickness ( 0 );
	joinText.setOutlineThickness ( 0 );
	backText.setOutlineThickness ( 0 );
	ipText.setOutlineThickness ( 0 );
	waitingText.setOutlineThickness ( 0 );
	connectingText.setOutlineThickness ( 0 );

	localText.setPosition ( 200, 625 );
	hostText.setPosition ( 200, 700 );
	joinText.setPosition ( 200, 775 );
	backText.setPosition ( 200, 850 );
	ipText.setPosition ( 1000, 625 );
	waitingText.setPosition ( 1000, 1000 );
	connectingText.setPosition ( 1000, 625 );
}

void ConnectionMenuState::draw()
{
	window.clear();
	window.draw ( backgroundSprite );
	switch ( menuState) {
	case MenuState::BaseMenu:
		window.draw ( localText );
		window.draw ( hostText );
		window.draw ( joinText );
		window.draw ( backText );
		break;

	case MenuState::ServerMenu:
		window.draw ( ipText );
		window.draw ( waitingText );
		window.draw ( backText );
		break;

	case MenuState::ClientMenu:
		window.draw ( connectingText );
		window.draw ( backText );
		break;
	}
	window.display();
}

void ConnectionMenuState::handleEvents()
{
	sf::Event event;
	while ( window.pollEvent ( event ) ) {
		switch ( event.type ) {

		//Close the window if the close button has been pressed
		case sf::Event::Closed:
			window.close();
			break;

		//Check whether anything clickable was clicked
		case sf::Event::MouseButtonPressed:
			if ( localText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.push ( std::make_unique<ModeMenuState> ( stateStack, window ) );
			} else if ( hostText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				menuState = MenuState::ServerMenu;
				std::string localHost = sf::IpAddress::LocalHost.toString();
				std::string localAddress = sf::IpAddress::getLocalAddress().toString();
				std::string publicAddress = sf::IpAddress::getPublicAddress().toString();
				ipText.setString (
					"Local Host: " + localHost + "\n" +
					"Local Address: " + localAddress + "\n" +
					"PublicAddress: " + publicAddress
				);
				if ( listener.listen ( 56274 ) != sf::Socket::Done ) {
					std::cout<<"Listening error"<<std::endl;
				}

				connectingThread = std::thread ( [this]() {
					/*if ( listener.accept ( socket ) == sf::Socket::Done ) {
						connectedToOpponent= true;
						std::cout<<"Connected successfully"<<std::endl;
					} else {
						std::cout<<"Accepting failed"<<std::endl;
					}*/
					connectedToOpponent = true;
				} );
			} else if ( joinText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				menuState = MenuState::ClientMenu;
				std::fstream stream;
				stream.open ( "ServerAddress.txt", std::fstream::in );
				std::string hostAddress;
				std::getline ( stream, hostAddress );
				connectingText.setString ( "Connecting to " + hostAddress );

				connectingThread = std::thread ( [this, hostAddress]() {
					if ( socket.connect ( hostAddress, 56274, sf::seconds ( 60.f ) ) ==sf::Socket::Done ) {
						connectedToOpponent = true;
						std::cout<<"Connected successfully"<<std::endl;
					} else {
						std::cout<<"Connecting failed"<<std::endl;
					}
				} );
			} else if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.pop ();
			}
			break;

		case sf::Event::Resized:
			SetAspectRatio( window, 16, 9 );
			break;

		default:
			break;
		}
	}
}

void ConnectionMenuState::update ( double )
{
	if ( localText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		localText.setOutlineThickness ( 5 );
	} else {
		localText.setOutlineThickness ( 0 );
	}

	if ( hostText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		hostText.setOutlineThickness ( 5 );
	} else {
		hostText.setOutlineThickness ( 0 );
	}

	if ( joinText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		joinText.setOutlineThickness ( 5 );
	} else {
		joinText.setOutlineThickness ( 0 );
	}

	if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		backText.setOutlineThickness ( 5 );
	} else {
		backText.setOutlineThickness ( 0 );
	}

	switch ( menuState ) {
	case MenuState::ServerMenu:
		if ( connectedToOpponent.load() ) {
			connectingThread.join();
			//stateStack.push ( std::make_unique<MatchState> ( stateStack, window, socket, MatchState::MultiplayerState::Server ) );
			connectedToOpponent.store ( false );
			menuState = MenuState::BaseMenu;
			//REMOVE WHEN MULTIPLAYER IS DONE
			socket.disconnect();
		}
		break;

	case MenuState::ClientMenu:
		if ( connectedToOpponent.load() ) {
			connectingThread.join();
			//stateStack.push ( std::make_unique<MatchState> ( stateStack, window, socket, MatchState::MultiplayerState::Client ) );
			connectedToOpponent.store ( false );
			menuState = MenuState::BaseMenu;
			//REMOVE WHEN MULTIPLAYER IS DONE
			socket.disconnect();
		}
		break;
	}
}
