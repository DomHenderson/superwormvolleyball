#include <memory>

#include <SFML/Window/Event.hpp>

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>

#include "aspectRatio.hpp"
#include "modeMenuState.hpp"
#include "titleScreenState.hpp"

//Construct the state
TitleScreenState::TitleScreenState ( StateStack& stateStack, sf::RenderWindow& window ) :
	stateStack ( stateStack ),
	window ( window )
{
	//Load textures
	backgroundTexture.loadFromFile ( "Sprites/Court.png" );

	//Assign textures to sprites
	backgroundSprite.setTexture ( backgroundTexture );

	//Load fonts
	startFont.loadFromFile ( "Fonts/Aileron-BlackItalic.otf" );
	titleFont.loadFromFile ( "Fonts/gooddog-plain.regular.ttf" );

	//Assign fonts to texts
	startText.setFont ( startFont );
	titleText.setFont( titleFont );

	//Set up texts
	startText.setString ( "Start Game" );
	startText.setCharacterSize ( 50 );
	startText.setFillColor ( sf::Color::Black );
	startText.setOutlineColor ( sf::Color::White );
	startText.setOutlineThickness ( 0 );
	startText.setPosition ( 960 - startText.getGlobalBounds().width/2, 625 );

	titleText.setString ( "SUPER WORM\n VOLLEYBALL" );
	titleText.setCharacterSize ( 225 );
	titleText.setFillColor ( sf::Color::Black );
	titleText.setPosition ( 960 - titleText.getGlobalBounds().width/2, 0 );
}

void TitleScreenState::draw()
{
	window.clear( sf::Color(8,0,16));
	window.draw ( backgroundSprite );
	window.draw ( startText );
	window.draw ( titleText );
	window.display();
}

void TitleScreenState::handleEvents()
{
	sf::Event event;
	while ( window.pollEvent ( event ) ) {
		switch ( event.type ) {

		//Close the window if the close button has been pressed
		case sf::Event::Closed:
			window.close();
			break;

		//Check whether anything clickable was clicked
		case sf::Event::MouseButtonPressed:
			if ( startText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.push ( std::make_unique<ModeMenuState> ( stateStack, window ) );
			}
			break;

		case sf::Event::Resized:
			SetAspectRatio( window, 16, 9 );
			break;

				default:
			break;
		}
	}
}

void TitleScreenState::update ( double )
{
	if ( startText.getGlobalBounds().contains( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
		startText.setOutlineThickness ( 5 );
	} else {
		startText.setOutlineThickness ( 0 );
	}
}
