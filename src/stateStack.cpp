#include <memory>
#include <utility>

#include "stateStack.hpp"

StateStack::StateStack() :
	popFlag ( false )
{
}

void StateStack::draw()
{
	stack.top()->draw();
}

void StateStack::handleEvents()
{
	stack.top()->handleEvents();
}

void StateStack::update ( double time )
{
	stack.top()->update ( time );
}

void StateStack::handleTransitions()
{
	if ( popFlag ) {
		stack.pop();
		popFlag = false;
	}

	if ( next ) {
		stack.push ( std::move ( next ) );
	}
}

void StateStack::pop()
{
	popFlag = true;
}

void StateStack::push ( std::unique_ptr<GameState>&& state )
{
	next = std::move ( state );
}

void StateStack::end()
{
	while ( !stack.empty() ) {
		stack.top().reset();
		stack.pop();
	}
}
