#include <cmath>
#include <iostream>
#include <utility>

#include "mathsUtilities.hpp"
#include "trail.hpp"

WormTrail::WormTrail ( sf::Color colour, float radius, float length ) :
	colour ( colour ),
	radius ( radius ),
	length ( length )
{

}

void WormTrail::add ( sf::Vector2f position, float angle )
{
	//New stamps are added at the back because it makes updating easier if they
	//are ordering in ascending order of time left
	trails.push_back ( std::pair<sf::VertexArray,double> ( sf::VertexArray(sf::TriangleFan, 34), duration ) );

	sf::VertexArray& v = trails[trails.size()-1].first;

	//Head semicircle
	for ( int i (0); i < 17; ++i ) {
		v[i].position.x = position.x + radius*std::cos(-angle + i*Pi/16);
		v[i].position.y = position.y + radius*std::sin(-angle + i*Pi/16);
		v[i].color = colour;
	}

	//Change position to be the centre of the tail
	position.x -= length*std::cos(-angle+Pi/2);
	position.y -= length*std::sin(-angle+Pi/2);

	//Tail semicirlce
	for ( int i (17); i < 34; ++i ) {
		v[i].position.x = position.x + radius*std::cos(-angle + i*Pi/16);
		v[i].position.y = position.y + radius*std::sin(-angle + i*Pi/16);
		v[i].color = colour;
	}
}

void WormTrail::draw ( sf::RenderTarget& target, sf::RenderStates states ) const
{
	//Adapt to SFML's negative y scale
	states.transform.scale ( sf::Vector2f ( 1, -1 ) );

	for ( auto& v: trails ) {
		target.draw ( v.first, states );
	}
}

void WormTrail::update ( double time )
{
	//Trails is in order of oldest to newest
	while ( !trails.empty() && trails[0].second <= 0.0 ) {
		trails.pop_front();
	}

	//Make the trails fade out
	for ( auto& p : trails ) {
		for ( int i (0); i < p.first.getVertexCount(); ++i ) {
			p.first[i].color.a = 255*p.second/duration;
		}
		p.second -= time;
	}
}

void WormTrail::clear()
{
	trails.clear();
}
