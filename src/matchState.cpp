#include <chrono>
#include <cmath>
#include <functional>
#include <iostream>
#include <optional>
#include <string>

#include <SFML/System/Vector2.hpp>

#include <SFML/Window/Event.hpp>

#include <SFML/Graphics/BlendMode.hpp>
#include <SFML/Graphics/Image.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/View.hpp>

#include <SFML/Audio/Music.hpp>

#include "aspectRatio.hpp"
#include "matchState.hpp"
#include "mathsUtilities.hpp"
#include "stateStack.hpp"

//Create custom worm colourings from a single template
sf::Image CreateWormImage ( bool left, sf::Color bodyColour, sf::Color eyeColour )
{
	sf::Image wormImage;

	wormImage.loadFromFile ( "Sprites/WhiteWorm.png" );

	for ( unsigned i (0); i < wormImage.getSize().x; ++i ) {
		for ( unsigned j (0); j < wormImage.getSize().y; ++j ) {
			if ( wormImage.getPixel ( i, j ) == sf::Color ( 255, 255, 255, 255 ) ) {
				wormImage.setPixel ( i, j, bodyColour );
			} else if ( wormImage.getPixel ( i, j ) == sf::Color ( 0, 0, 0, 255 ) ) {
				wormImage.setPixel ( i, j, eyeColour );
			}
		}
	}

	if ( !left ) {
		wormImage.flipHorizontally();
	}

	return wormImage;
}

sf::Image CreateWormSpriteSheet ( bool left, sf::Color bodyColour, sf::Color eyeColour )
{
	sf::Image wormImage;

	wormImage.loadFromFile ( "Sprites/WormPowerUpBlur.png" );

	for ( unsigned i (0); i < wormImage.getSize().x; ++i ) {
		for ( unsigned j (0); j < wormImage.getSize().y; ++j ) {
			if ( wormImage.getPixel ( i, j ) == sf::Color ( 118, 66, 138, 255 ) ) {
				wormImage.setPixel ( i, j, bodyColour );
			} else if ( wormImage.getPixel ( i, j ) == sf::Color ( 0, 0, 0, 255 ) ) {
				wormImage.setPixel ( i, j, eyeColour );
			}
		}
	}

	if ( !left ) {
		for ( int i = 0; i < 15; ++i ) {
			int spriteWidth = wormImage.getSize().x/15;
			for ( int x = 0; x < spriteWidth/2; ++x ) {
				for ( int y = 0; y < wormImage.getSize().y; ++y ) {
					sf::Color left = wormImage.getPixel(i*spriteWidth + x,y);
					sf::Color right = wormImage.getPixel((i+1)*spriteWidth - x - 1,y);
					wormImage.setPixel(i*spriteWidth + x, y, right );
					wormImage.setPixel((i+1)*spriteWidth - x - 1, y, left );
				}
			}
		}
	}

	return wormImage;
}

MatchState::ScoreBoard::ScoreBoard ( std::function<void(void)> newPoint, sf::Color leftColour, sf::Color rightColour ) :
	newPoint ( newPoint ),
	rightServe ( false ),
	leftPoints ( 0 ),
	leftBounces ( 0 ),
	rightPoints ( 0 ),
	rightBounces ( 0 ),
	leftColour ( leftColour ),
	rightColour ( rightColour )
{
	for ( auto& x: pointCircles ) {
		x.setPointCount ( 32 );
		x.setRadius ( 10 );
		x.setOrigin ( sf::Vector2f ( 10, 10 ) );
		x.setOutlineThickness ( 4 );
		x.setFillColor ( sf::Color::Transparent );
		x.setOutlineColor ( sf::Color ( 128,128,128 ) );
	}

	for ( auto& x: bounceCircles ) {
		x.setPointCount ( 32 );
		x.setRadius ( 10 );
		x.setOrigin ( sf::Vector2f ( 10, 10 ) );
		x.setOutlineThickness ( 4 );
		x.setFillColor ( sf::Color::Transparent );
		x.setOutlineColor ( sf::Color ( 128,128,128 ) );
	}

	pointCircles[4].setRadius ( 20 );
	pointCircles[4].setOrigin ( 20, 20 );
	bounceCircles[2].setRadius ( 15 );
	bounceCircles[2].setOrigin ( 15, 15 );

	pointCircles[0].setPosition ( 790, 30 );
	pointCircles[1].setPosition ( 830, 30 );
	pointCircles[2].setPosition ( 870, 30 );
	pointCircles[3].setPosition ( 910, 30 );
	pointCircles[4].setPosition ( 960, 30 );
	pointCircles[5].setPosition ( 1010, 30 );
	pointCircles[6].setPosition ( 1050, 30 );
	pointCircles[7].setPosition ( 1090, 30 );
	pointCircles[8].setPosition ( 1130, 30 );

	bounceCircles[0].setPosition ( 875, 85 );
	bounceCircles[1].setPosition ( 915, 85 );
	bounceCircles[2].setPosition ( 960, 85 );
	bounceCircles[3].setPosition ( 1005, 85 );
	bounceCircles[4].setPosition ( 1045, 85 );

	victoryFont.loadFromFile ( "Fonts/Aileron-BlackItalic.otf" );
	victoryText.setFont ( victoryFont );
	victoryText.setCharacterSize ( 70 );
	victoryText.setFillColor ( sf::Color::Transparent );
	victoryText.setPosition ( 960 - victoryText.getGlobalBounds().width/2.0, 400 - victoryText.getGlobalBounds().height/2.0 );
}

void MatchState::ScoreBoard::addLeftBounce()
{
	if ( leftPoints < 5 && rightPoints < 5 ) {
		++leftBounces;
		if ( leftBounces >= 3 ) {
			bounceCircles[2].setFillColor ( rightColour );
			++leftPoints;
			if ( leftPoints <= 5 ) {
				pointCircles[9-leftPoints].setFillColor ( rightColour );
			}
			if ( leftPoints == 5 ) {
				victoryText.setString ( "RIGHT PLAYER WINS!" );
				victoryText.setFillColor ( sf::Color::Black );
				victoryText.setPosition ( 960 - victoryText.getGlobalBounds().width/2.0, 400 - victoryText.getGlobalBounds().height/2.0 );
			}
			newPoint();
		} else {
			bounceCircles[5-leftBounces].setFillColor ( rightColour );
		}
	}
}

void MatchState::ScoreBoard::addRightBounce()
{
	if ( leftPoints < 5 && rightPoints < 5 ) {
		++rightBounces;
		if ( rightBounces >= 3 ) {
			bounceCircles[2].setFillColor ( leftColour );
			++rightPoints;
			if ( rightPoints <= 5 ) {
				pointCircles[rightPoints-1].setFillColor ( leftColour );
			}
			if ( rightPoints == 5 ) {
				victoryText.setString ( "LEFT PLAYER WINS!" );
				victoryText.setFillColor ( sf::Color::Black );
				victoryText.setPosition ( 960 - victoryText.getGlobalBounds().width/2.0, 400 - victoryText.getGlobalBounds().height/2.0 );
			}
			newPoint();
		} else {
			bounceCircles[rightBounces-1].setFillColor ( leftColour );
		}
	}
}


MatchState::Court::Court ( sf::FloatRect area, float groundLevel, float grassDepth, float netHeight, float netRadius ) :
	area ( area ),
	groundLevel ( groundLevel ),
	netHeight ( netHeight ),
	netRadius ( netRadius ),
	skyRect ( sf::Vector2f ( area.width, area.height / 2 - groundLevel ) ),
	grassRect ( sf::Vector2f ( area.width, grassDepth ) ),
	groundRect ( sf::Vector2f ( area.width, groundLevel - grassDepth - area.top + area.height ) ),
	netRect ( sf::Vector2f ( 2*netRadius, netHeight+grassDepth/2 ) ),
	netTopCircle ( netRadius, 32 ),
	netBottomCircle ( netRadius, 32 )
{
	skyRect.setPosition ( area.left, -area.top );
	skyRect.setFillColor ( sf::Color(50, 210, 210) );
	grassRect.setPosition ( area.left, -groundLevel );
	grassRect.setFillColor ( sf::Color::Green );
	groundRect.setPosition ( area.left, grassDepth - groundLevel );
	groundRect.setFillColor ( sf::Color ( 153, 77, 0 ) );
	netRect.setPosition ( -netRadius, -netHeight - groundLevel );
	netRect.setFillColor ( sf::Color ( 204, 204, 255 ) );
	netTopCircle.setPosition ( -netRadius, -netHeight - netRadius - groundLevel );
	netTopCircle.setFillColor ( sf::Color ( 204, 204, 255 ) );
	netBottomCircle.setPosition ( -netRadius, - netRadius - groundLevel );
	netBottomCircle.setFillColor ( sf::Color ( 204, 204, 255 ) );
}

MatchState::Worm::Worm ( sf::Vector2f position, ControlScheme controlScheme, sf::Color bodyColour, sf::Color eyeColour ) :
	radius ( 15.f ),
	length ( 90.f ),
	direction ( 0.0 ),
	inGround ( true ),
	headPosition ( position ),
	headVelocity ( 0.0, 0.0 ),
	tailPosition ( position.x, position.y - length ),
	tailVelocity ( 0.0, 0.0 ),
	controller ( GetController ( controlScheme ) ),
	frame ( 0.0 ),
	colour ( bodyColour ),
	trail ( {48,20,0}, 15.f, 90.f )
{
	//texture.loadFromImage ( CreateWormImage( position.x < 0.0, bodyColour, eyeColour ) );
	textureSheet.loadFromImage ( CreateWormSpriteSheet ( position.x < 0.0, bodyColour, eyeColour ) );
	sheetSprite.setTexture ( textureSheet );
	sheetSprite.setTextureRect ( sf::IntRect(0,0,0,130));
	texture.create(40,130);
	sprite.setTexture ( texture.getTexture() );
	sprite.setOrigin ( 20, 20 );

	//netCollisionSoundBuffer.loadFromFile ( "Sounds/WormNetCollision.ogg" );
	//netCollisionSound.setBuffer ( netCollisionSoundBuffer );
}

MatchState::Worm::Controller MatchState::Worm::GetController ( ControlScheme controls )
{
	switch ( controls ) {
	case ControlScheme::Wasd:
		return Controller {
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::W ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::A ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::D ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::S )
		};
		break;

	case ControlScheme::ArrowKeys:
		return Controller {
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::Up ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::Left ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::Right ),
			std::bind ( sf::Keyboard::isKeyPressed, sf::Keyboard::Down )
		};
		break;
	}
}

void MatchState::Worm::PrepareTexture()
{
	if ( frame > 14 ) frame -= 4;
	int before = std::floor ( frame );
	int after = std::ceil ( frame );
	if ( before < 0 ) before = 0;
	if ( after > 14 ) after = 14;
	if ( before == after ) {
		sheetSprite.setTextureRect(sf::IntRect(40*before,0,40,130));
		sheetSprite.setColor({255,255,255,255});
		texture.clear(sf::Color::Transparent);
		texture.draw(sheetSprite);
		texture.display();
	} else {
		unsigned beforeAlpha = (frame - before)*255;
		sheetSprite.setTextureRect(sf::IntRect(40*before,0,40,130));
		sheetSprite.setColor({255,255,255,beforeAlpha});
		texture.clear(sf::Color::Transparent);
		texture.draw(sheetSprite,sf::RenderStates(sf::BlendAdd));
		sheetSprite.setTextureRect(sf::IntRect(40*after,0,40,130));
		sheetSprite.setColor({255,255,255,255-beforeAlpha});
		texture.draw(sheetSprite,sf::RenderStates(sf::BlendAdd));
		texture.display();
	}
}

MatchState::PowerUI::PowerUI ( Ball& b, Worm& left, Worm& right, ScoreBoard& score, float chargeSpeed ) :
	speed ( chargeSpeed ), leftCharge (0), rightCharge (0), ball ( b ), leftWorm ( left ), rightWorm ( right ), score ( score ),
	leftBar ( {10,0} ), leftOutline ( {14,54} ), rightBar ( { 10,0 } ), rightOutline ( { 14,54} ),
	useLeft ( false ), useRight ( false ), leftTime (0.0), rightTime (0.0), range ( 1000 ), strength ( 10 ), freezeTime ( 1.0 ),
	generator (std::chrono::system_clock::now().time_since_epoch().count()), distribution(1,3)
{
	leftBar.setOrigin(5,0);
	rightBar.setOrigin(5,0);

	leftOutline.setOrigin(7,52);
	rightOutline.setOrigin(7,52);

	leftBar.setPosition(50,100);
	rightBar.setPosition(1770,100);
	leftOutline.setPosition(50,100);
	rightOutline.setPosition(1770,100);

	leftOutline.setFillColor(sf::Color(128,128,128));
	rightOutline.setFillColor(sf::Color(128,128,128));
}

void MatchState::PowerUI::update( double time )
{
	int pointDifference = score.leftPoints*3+score.leftBounces-score.rightPoints*3-score.rightBounces;
	leftCharge += time*speed*(leftWorm.headPosition.x > 0 ? 1.5 : 1)*(ball.position.x > 0 ? 2 : 1)*(1+pointDifference*0.035);
	rightCharge += time*speed*(rightWorm.headPosition.x < 0 ? 1.5 : 1)*(ball.position.x < 0 ? 2 : 1)*(1-pointDifference*0.035);

	if ( leftCharge > 1.0 ) leftCharge = 1.0;
	if ( rightCharge > 1.0 ) rightCharge = 1.0;

	if ( leftCharge > 0.5 ) {
		leftBar.setFillColor(sf::Color::White);
		if ( leftPower == Power::None ) {
			leftWorm.charged = true;
			int newPowerId = distribution(generator);
			leftPower = static_cast<Power>(newPowerId);
			switch ( leftPower ) {
			case Power::Push:
				leftOutline.setFillColor(sf::Color::Magenta);
				break;
			case Power::Pull:
				leftOutline.setFillColor(sf::Color::Yellow);
				break;
			case Power::Freeze:
				leftOutline.setFillColor(sf::Color::Cyan);
				break;
			}
		}
	} else {
		leftBar.setFillColor(leftWorm.colour);
		leftOutline.setFillColor(sf::Color(128,128,128));
		leftWorm.charged = false;
	}

	if ( rightCharge > 0.5 ) {
		rightBar.setFillColor(sf::Color::White);
		if ( rightPower == Power::None ) {
			rightWorm.charged = true;
			int newPowerId = distribution(generator);
			rightPower = static_cast<Power>(newPowerId);
			switch ( rightPower ) {
			case Power::Push:
				rightOutline.setFillColor(sf::Color::Magenta);
				break;
			case Power::Pull:
				rightOutline.setFillColor(sf::Color::Yellow);
				break;
			case Power::Freeze:
				rightOutline.setFillColor(sf::Color::Cyan);
				break;
			}
		}
	} else {
		rightBar.setFillColor(rightWorm.colour);
		rightOutline.setFillColor(sf::Color(128,128,128));
		rightWorm.charged =false;
	}

	leftBar.setSize({10,leftCharge*-50});
	rightBar.setSize({10,rightCharge*-50});

	if ( useLeft ) {
		useLeft = false;
		if ( leftCharge > 0.5 ) {
			switch ( leftPower ) {
			case Power::Push: {
				float distance = std::sqrt(SquareDistance(leftWorm.headPosition, ball.position));
				float power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				ball.velocity += power*(ball.position-leftWorm.headPosition)/Modulus(ball.position-leftWorm.headPosition);

				distance = SquareDistance(leftWorm.tailPosition, ball.position);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				ball.velocity += power*(ball.position-leftWorm.tailPosition)/Modulus(ball.position-leftWorm.tailPosition);

				distance = std::sqrt(SquareDistance(leftWorm.headPosition, rightWorm.headPosition));
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				rightWorm.headVelocity += power*(rightWorm.headPosition-leftWorm.headPosition)/Modulus(rightWorm.headPosition-leftWorm.headPosition);
				rightWorm.tailVelocity += power*(rightWorm.headPosition-leftWorm.headPosition)/Modulus(rightWorm.headPosition-leftWorm.headPosition);

				distance = SquareDistance(leftWorm.tailPosition, rightWorm.headPosition);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				rightWorm.headVelocity += power*(rightWorm.headPosition-leftWorm.tailPosition)/Modulus(rightWorm.headPosition-leftWorm.tailPosition);
				rightWorm.tailVelocity += power*(rightWorm.headPosition-leftWorm.tailPosition)/Modulus(rightWorm.headPosition-leftWorm.tailPosition);
				leftCharge = 0;
				leftPower = Power::None;
				} break;

			case Power::Pull: {
				float distance = std::sqrt(SquareDistance(leftWorm.headPosition, ball.position));
				float power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				ball.velocity -= power*(ball.position-leftWorm.headPosition)/Modulus(ball.position-leftWorm.headPosition);

				distance = SquareDistance(leftWorm.tailPosition, ball.position);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				ball.velocity -= power*(ball.position-leftWorm.tailPosition)/Modulus(ball.position-leftWorm.tailPosition);

				distance = std::sqrt(SquareDistance(leftWorm.headPosition, rightWorm.headPosition));
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				rightWorm.headVelocity -= power*(rightWorm.headPosition-leftWorm.headPosition)/Modulus(rightWorm.headPosition-leftWorm.headPosition);
				rightWorm.tailVelocity -= power*(rightWorm.headPosition-leftWorm.headPosition)/Modulus(rightWorm.headPosition-leftWorm.headPosition);

				distance = SquareDistance(leftWorm.tailPosition, rightWorm.headPosition);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= leftCharge;
				power *= strength;

				rightWorm.headVelocity -= power*(rightWorm.headPosition-leftWorm.tailPosition)/Modulus(rightWorm.headPosition-leftWorm.tailPosition);
				rightWorm.tailVelocity -= power*(rightWorm.headPosition-leftWorm.tailPosition)/Modulus(rightWorm.headPosition-leftWorm.tailPosition);
				leftCharge = 0;
				leftPower = Power::None;
				} break;

			case Power::Freeze:
				leftTime = freezeTime*(leftCharge-0.5)*2;
				ball.velocity = {0,0};
				rightWorm.headVelocity = {0,0};
				rightWorm.tailVelocity = {0,0};
				rightDirection = rightWorm.direction;
				leftCharge = 0;
				useLeft = true;
				break;
			}
		} else if ( leftTime > 0.0 ) {
			ball.velocity = {0,0};
			rightWorm.headVelocity = {0,0};
			rightWorm.tailVelocity = {0,0};
			rightWorm.direction = rightDirection;
			rightWorm.tailPosition.x = rightWorm.headPosition.x - rightWorm.length * std::sin ( rightDirection );
			rightWorm.tailPosition.y = rightWorm.headPosition.y - rightWorm.length * std::cos ( rightDirection );
			leftTime -= time;
			if ( leftTime < 0.0 ) {
				leftTime = 0.0;
				leftPower = Power::None;
			} else {
				useLeft = true;
			}
		}
	}

	if ( useRight ) {
		useRight = false;
		if ( rightCharge > 0.5 ) {
			switch ( rightPower ) {
			case Power::Push: {
				float distance = std::sqrt(SquareDistance(rightWorm.headPosition, ball.position));
				float power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				ball.velocity += power*(ball.position-rightWorm.headPosition)/Modulus(ball.position-rightWorm.headPosition);

				distance = SquareDistance(rightWorm.tailPosition, ball.position);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				ball.velocity += power*(ball.position-rightWorm.tailPosition)/Modulus(ball.position-rightWorm.tailPosition);

				distance = std::sqrt(SquareDistance(rightWorm.headPosition, leftWorm.headPosition));
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				leftWorm.headVelocity += power*(leftWorm.headPosition-rightWorm.headPosition)/Modulus(leftWorm.headPosition-rightWorm.headPosition);
				leftWorm.tailVelocity += power*(leftWorm.headPosition-rightWorm.headPosition)/Modulus(leftWorm.headPosition-rightWorm.headPosition);

				distance = SquareDistance(rightWorm.tailPosition, leftWorm.headPosition);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				leftWorm.headVelocity += power*(leftWorm.headPosition-rightWorm.tailPosition)/Modulus(leftWorm.headPosition-rightWorm.tailPosition);
				leftWorm.tailVelocity += power*(leftWorm.headPosition-rightWorm.tailPosition)/Modulus(leftWorm.headPosition-rightWorm.tailPosition);
				rightCharge = 0;
				rightPower = Power::None;
				} break;

			case Power::Pull: {
				float distance = std::sqrt(SquareDistance(rightWorm.headPosition, ball.position));
				float power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				ball.velocity -= power*(ball.position-rightWorm.headPosition)/Modulus(ball.position-rightWorm.headPosition);

				distance = SquareDistance(rightWorm.tailPosition, ball.position);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				ball.velocity -= power*(ball.position-rightWorm.tailPosition)/Modulus(ball.position-rightWorm.tailPosition);

				distance = std::sqrt(SquareDistance(rightWorm.headPosition, leftWorm.headPosition));
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				leftWorm.headVelocity -= power*(leftWorm.headPosition-rightWorm.headPosition)/Modulus(leftWorm.headPosition-rightWorm.headPosition);
				leftWorm.tailVelocity -= power*(leftWorm.headPosition-rightWorm.headPosition)/Modulus(leftWorm.headPosition-rightWorm.headPosition);

				distance = SquareDistance(rightWorm.tailPosition, leftWorm.headPosition);
				power = range-distance;
				if ( power < 0 ) power = 0;
				if ( power > 200 ) power = 200;
				power *= rightCharge;
				power *= strength;

				leftWorm.headVelocity -= power*(leftWorm.headPosition-rightWorm.tailPosition)/Modulus(leftWorm.headPosition-rightWorm.tailPosition);
				leftWorm.tailVelocity -= power*(leftWorm.headPosition-rightWorm.tailPosition)/Modulus(leftWorm.headPosition-rightWorm.tailPosition);
				rightCharge = 0;
				rightPower = Power::None;
				} break;

			case Power::Freeze:
				rightTime = freezeTime*(rightCharge-0.5)*2;
				ball.velocity = {0,0};
				leftWorm.headVelocity = {0,0};
				leftWorm.tailVelocity = {0,0};
				leftDirection = leftWorm.direction;
				rightCharge = 0;
				useRight = true;
				break;
			}
		} else if ( rightTime > 0.0 ) {
			ball.velocity = {0,0};
			leftWorm.headVelocity = {0,0};
			rightWorm.tailVelocity = {0,0};
			leftWorm.direction = leftDirection;
			leftWorm.tailPosition.x = leftWorm.headPosition.x - leftWorm.length * std::sin ( leftDirection );
			leftWorm.tailPosition.y = leftWorm.headPosition.y - leftWorm.length * std::cos ( leftDirection );
			rightTime -= time;
			if ( rightTime < 0.0 ) {
				rightTime = 0.0;
				rightPower = Power::None;
			} else {
				useRight = true;
			}
		}
	}
}

MatchState::MatchState ( StateStack& stateStack, sf::RenderWindow& window, float chargeRate ):
	stateStack ( stateStack ),
	runningTime ( 0.0 ),
	scoreBoard (
		[this] (void) {
			leftWorm.headPosition = { -800.f, -600.f };
			leftWorm.headVelocity = { 0.f, 0.f };
			leftWorm.tailPosition = { -800.f, -690.f };
			leftWorm.tailVelocity = { 0.f, 0.f };
			leftWorm.direction = 0;
			leftWorm.inGround = true;
			leftWorm.trail.clear();

			rightWorm.headPosition = { 800.f, -600.f };
			rightWorm.headVelocity = { 0.f, 0.f };
			rightWorm.tailPosition = { 800.f, -690.f };
			rightWorm.tailVelocity = { 0.f, 0.f };
			rightWorm.direction = 0;
			rightWorm.inGround = true;
			rightWorm.trail.clear();

			ball.position = { 0.f, court.groundLevel + court.netHeight + court.netRadius + ball.radius + 10.f };
			if ( scoreBoard.leftPoints < 5 && scoreBoard.rightPoints < 5 ) {
				if ( scoreBoard.rightServe ) {
					ball.velocity = { 200.f, 250.f };
					scoreBoard.rightServe = false;
				} else {
					ball.velocity = { -200.f, 250.f };
					scoreBoard.rightServe = true;
				}
			} else {
				ball.velocity = { 0.f, 0.f };
			}

			scoreBoard.leftBounces = 0;
			scoreBoard.rightBounces = 0;
			for ( auto&x: scoreBoard.bounceCircles ) {
				x.setFillColor ( sf::Color::Transparent );
			}
		},
		sf::Color(255,0,0,255),
		sf::Color(0,0,255,255)
	),
	court (
		sf::FloatRect ( -1920.f, 1080.f, 3840.f, 2160.f ),
		-400.f,
		16.f,
		200.f,
		10.f
	),
	ball { sf::Vector2f(0.f, -148.f), sf::Vector2f(200.f, 250.f), 40.f, sf::CircleShape ( 40.f, 32 ) },
	leftWorm ( sf::Vector2f ( -800.f, -600.f ), Worm::ControlScheme::Wasd, sf::Color ( 255, 0, 0, 255 ), sf::Color ( 0, 255, 0, 255 ) ),
	rightWorm ( sf::Vector2f ( 800.f, -600.f ), Worm::ControlScheme::ArrowKeys, sf::Color ( 0, 0, 255, 255 ), sf::Color ( 255, 255, 0, 255 ) ),
	window ( window )
{
	if ( chargeRate > 0.f ) {
		powerUI.emplace( ball, leftWorm, rightWorm, scoreBoard, chargeRate );
	}

	ball.circle.setFillColor ( sf::Color::Yellow );
	ball.hitSoundBuffer.loadFromFile ( "Sounds/ballHit.ogg" );
	ball.hitSound.setBuffer ( ball.hitSoundBuffer );
	ball.hitVolume = 100.f;
	ball.hitSound.setVolume ( 40 );

	groundTrack = std::make_unique<sf::Music>();
	groundTrack->openFromFile ( "Sounds/LowPinkNoise.ogg" );
	groundTrack->setLoop ( true );
	groundTrack->setVolume ( 0 );
	groundTrack->play();

	menuFont.loadFromFile ( "Fonts/Aileron-BlackItalic.otf") ;
	againText.setString ( "Play again" );
	backText.setString ( "Back to menu" );
	againText.setFont ( menuFont );
	backText.setFont ( menuFont );
	againText.setCharacterSize ( 50 );
	backText.setCharacterSize ( 50 );
	againText.setFillColor ( sf::Color::Black );
	backText.setFillColor ( sf::Color::Black );
	againText.setOutlineColor ( sf::Color::White );
	backText.setOutlineColor ( sf::Color::White );
	againText.setPosition ( 960 - againText.getGlobalBounds().width/2.0, 600 );
	backText.setPosition ( 960 - backText.getGlobalBounds().width/2.0, 675 );

	renderTexture.create ( 1920, 1080 );
}

void MatchState::draw()
{
	ball.circle.setPosition ( sf::Vector2f ( ball.position.x - ball.radius, -ball.position.y-ball.radius ) );

	leftWorm.sprite.setPosition ( sf::Vector2f ( leftWorm.headPosition.x, -leftWorm.headPosition.y ) );
	leftWorm.sprite.setRotation ( leftWorm.direction * 180 / Pi );
	leftWorm.PrepareTexture();

	rightWorm.sprite.setPosition ( sf::Vector2f ( rightWorm.headPosition.x, -rightWorm.headPosition.y ) );
	rightWorm.sprite.setRotation ( rightWorm.direction * 180 / Pi );
	rightWorm.PrepareTexture();

	sf::View mainView ( sf::Vector2f(0,0), sf::Vector2f( 3840, 2160 ) );
	renderTexture.setView ( mainView );
	sf::View guiView ( renderTexture.getDefaultView() );

	renderTexture.clear();
	renderTexture.draw ( court.groundRect );
	renderTexture.draw ( leftWorm.trail );
	renderTexture.draw ( rightWorm.trail );
	renderTexture.draw ( court.skyRect );
	renderTexture.draw ( court.grassRect );
	renderTexture.draw ( court.netRect );
	renderTexture.draw ( court.netTopCircle );
	renderTexture.draw ( court.netBottomCircle );
	renderTexture.draw ( ball.circle );
	renderTexture.draw ( leftWorm.sprite );
	renderTexture.draw ( rightWorm.sprite );
	renderTexture.setView ( guiView );
	for ( auto& x: scoreBoard.pointCircles ) {
		renderTexture.draw ( x );
	}
	for ( auto& x: scoreBoard.bounceCircles ) {
		renderTexture.draw ( x );
	}
	if ( powerUI ) {
		renderTexture.draw(powerUI->leftOutline);
		renderTexture.draw(powerUI->leftBar);
		renderTexture.draw(powerUI->rightOutline);
		renderTexture.draw(powerUI->rightBar);
	}
	renderTexture.draw ( scoreBoard.victoryText );
	if ( scoreBoard.leftPoints == 5 || scoreBoard.rightPoints == 5 ) {
		renderTexture.draw ( againText );
		renderTexture.draw ( backText );
	}
	renderTexture.display();

	sf::Sprite renderSprite;
	renderSprite.setTexture(renderTexture.getTexture());

	window.clear ( sf::Color(8,0,16) );
	window.draw(renderSprite);
	window.display();
}

void MatchState::handleEvents()
{
	sf::Event event;
	while ( window.pollEvent ( event ) ) {
		switch ( event.type ) {

		//Close the window if the close button has been pressed
		case sf::Event::Closed:
			groundTrack->stop();
			groundTrack.reset();
			window.close();
			break;

		case sf::Event::Resized:
			SetAspectRatio( window, 16, 9 );
			break;

		case sf::Event::KeyPressed:
			if ( powerUI ) {
				if ( event.key.code == sf::Keyboard::T ) {
					powerUI->useLeftPower();
				} else if ( event.key.code == sf::Keyboard::Slash ) {
					powerUI->useRightPower();
				}
			}
			break;

		case sf::Event::MouseButtonPressed:
			if ( againText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.pop();
				std::cout<< (powerUI ? std::to_string(powerUI->getChargeSpeed()) : "None") << std::endl;
				stateStack.push ( std::make_unique<MatchState> ( stateStack, window, powerUI ? powerUI->getChargeSpeed() : 0.0f ) );
			} else if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
				stateStack.pop();
			}

		default:
			break;
		}
	}
}

void MatchState::update ( double time )
{
	if ( scoreBoard.leftPoints == 5 || scoreBoard.rightPoints == 5 ) {
		if ( againText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
			againText.setOutlineThickness ( 5 );
		} else {
			againText.setOutlineThickness ( 0 );
		}

		if ( backText.getGlobalBounds().contains ( window.mapPixelToCoords ( sf::Mouse::getPosition ( window ) ) ) ) {
			backText.setOutlineThickness ( 5 );
		} else {
			backText.setOutlineThickness ( 0 );
		}
	}

	//Left worm physics
	leftWorm.update ( time, court );

	//Right worm physics
	rightWorm.update ( time, court );

	//Ball physics
	ball.update ( time, court, scoreBoard, leftWorm, rightWorm );

	if ( powerUI ) powerUI->update ( time );

	//Sound effects
	float lgroundVolume ( 0.f );

	lgroundVolume += ModCrossProduct ( leftWorm.headVelocity, { leftWorm.headPosition.x - leftWorm.tailPosition.x, leftWorm.headPosition.y - leftWorm.tailPosition.y } );
	//lgroundVolume += ModCrossProduct ( leftWorm.tailVelocity, { leftWorm.headPosition.x - leftWorm.tailPosition.x, leftWorm.headPosition.y - leftWorm.tailPosition.y } );

	lgroundVolume *= leftWorm.inGround;

	float rgroundVolume ( 0.f );

	rgroundVolume += ModCrossProduct ( rightWorm.headVelocity, { rightWorm.headPosition.x - rightWorm.tailPosition.x, rightWorm.headPosition.y - rightWorm.tailPosition.y } );
	//rgroundVolume += ModCrossProduct ( rightWorm.tailVelocity, { rightWorm.headPosition.x - rightWorm.tailPosition.x, rightWorm.headPosition.y - rightWorm.tailPosition.y } );

	rgroundVolume *= rightWorm.inGround;

	groundTrack->setVolume ( std::sqrt ( lgroundVolume + rgroundVolume ) / 25.f );

	runningTime += time;
}

void MatchState::Ball::update ( double time, Court& court, ScoreBoard& scoreBoard, Worm& leftWorm, Worm& rightWorm )
{
	//Gravity
	velocity.y -= 100 * time;

	//Air resistance
	double v = Modulus ( velocity ) / 1800;
	double coefficient = 1/(v*v*v + 1);
	velocity.x *= std::pow ( coefficient, time );
	velocity.y *= std::pow ( coefficient, time );

	//Velocity
	position.x += velocity.x * time;
	position.y += velocity.y * time;

	//Collision with the ground
	if ( position.y - radius <= court.groundLevel ) {
		position.y = 2*radius + 2*court.groundLevel - position.y;
		if ( velocity.y < 0.f ) {
			velocity.y *= -0.85;
		}
		if ( position.x > 0.f ) {
			scoreBoard.addRightBounce();
		} else {
			scoreBoard.addLeftBounce();
		}
		hitSound.play();
	}

	//Collision with the right wall
	if ( position.x + radius >= court.area.left+court.area.width ) {
		position.x = 2*(court.area.left+court.area.width) - 2*radius - position.x;
		if ( velocity.x > 0.f ) {
			velocity.x *= -0.95;
		}
		hitSound.play();
	}

	//Collision with the left wall
	if ( position.x - radius <= court.area.left ) {
		position.x = -position.x + 2*court.area.left + 2*radius;
		if ( velocity.x < 0.f ) {
			velocity.x *= -0.95;
		}
		hitSound.play();
	}

	//Collision with the ceiling
	if ( position.y + radius >= court.area.top ) {
		position.y = -position.y + 2*court.area.top - 2*radius;
		if ( velocity.y > 0.f ) {
			velocity.y *= -0.6;
		}
		hitSound.play();
	}

	//Collision with the left hand side of the net
	if ( position.x < 0.f && position.x + radius > -court.netRadius && position.y < court.netHeight + court.groundLevel ) {
		position.x = -position.x - 2*radius - 2*court.netRadius;
		if ( velocity.x > 0.f ) {
			velocity.x *= -0.95;
		}
		hitSound.play();
	}

	//Collision with the right hand side of the net
	if ( position.x > 0.f && position.x - radius < court.netRadius && position.y < court.netHeight + court.groundLevel ) {
		position.x = -position.x + 2*radius + 2*court.netRadius;
		if ( velocity.x < 0.f ) {
			velocity.x *= -0.95;
		}
		hitSound.play();
	}

	//Collision with the top of the net
	if ( position.x*position.x + (position.y-court.netHeight-court.groundLevel)*(position.y-court.netHeight-court.groundLevel) <= (radius+court.netRadius)*(radius+court.netRadius) ) {
		sf::Vector2f normal ( position.x, position.y - court.netHeight - court.groundLevel );
		float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
		normal.x /= magnitude;
		normal.y /= magnitude;

		if ( magnitude < radius + court.netRadius ) {
			magnitude -= radius + court.netRadius;
			position.x -= 2 * magnitude * normal.x;
			position.y -= 2 * magnitude * normal.y;
		}

		float dotProduct = normal.x*velocity.x + normal.y*velocity.y;

		velocity.x -= 1.95 * normal.x * ( dotProduct );
		velocity.y -= 1.95 * normal.y * ( dotProduct );
		hitSound.play();
	}

	HandleWormCollision ( leftWorm );

	HandleWormCollision( rightWorm );
}

void MatchState::Ball::HandleWormCollision ( Worm& worm )
{
	bool behindHead ( false );
	bool behindTail ( false );
	if (
		DotProduct (
			sf::Vector2f ( position.x - worm.headPosition.x, position.y - worm.headPosition.y ),
			sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y )
		) > 0
	) {
		behindHead = true;
	}

	if (
		DotProduct (
			sf::Vector2f ( position.x - worm.tailPosition.x, position.y - worm.tailPosition.y ),
			sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y )
		) > 0
	) {
		behindTail = true;
	}

	if ( behindTail ) {
		if ( SquareDistance ( position, worm.tailPosition ) <= ( radius + worm.radius ) * ( radius + worm.radius ) ) {
			sf::Vector2f normal ( position.x - worm.tailPosition.x, position.y - worm.tailPosition.y );
			float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
			normal.x /= magnitude;
			normal.y /= magnitude;

			if ( magnitude < radius + worm.radius ) {
				magnitude -= radius + worm.radius;
				position.x -= 2 * magnitude * normal.x;
				position.y -= 2 * magnitude * normal.y;
			}

			float dotProduct = normal.x*velocity.x + normal.y*velocity.y;

			if ( dotProduct < 0 ) {
				velocity.x -= 1.95 * normal.x * ( dotProduct );
				velocity.y -= 1.95 * normal.y * ( dotProduct );
			}

			velocity.x += DotProduct ( worm.tailVelocity, normal ) * normal.x;
			velocity.y += DotProduct ( worm.tailVelocity, normal ) * normal.y;

			hitSound.play();
		}
	} else if ( behindHead ) {

		//Calculate the perpendicular distance from the ball to the worm
		float distance =
		ModCrossProduct (
			sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y ),
			sf::Vector2f ( position.x - worm.headPosition.x, position.y - worm.headPosition.y )
		) / Modulus (
			sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y )
		);

		//The collision only occurs if this distance is less than their combined radii
		if (
			distance < radius + worm.radius
		) {

			//Calculate the normalised normal vector of the worm's surfce
			sf::Vector2f normal ( worm.tailPosition.y - worm.headPosition.y, worm.headPosition.x - worm.tailPosition.x );
			float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
			normal.x /= magnitude;
			normal.y /= magnitude;

			//The normal must point out of the worm towards the ball
			if ( DotProduct ( normal, sf::Vector2f ( position.x - worm.headPosition.x, position.y - worm.headPosition.y ) ) < 0.f ) {
				normal.x *= -1;
				normal.y *= -1;
			}

			if ( distance < radius + worm.radius ) {

				//Distance is now the overlap between the two
				distance -= radius + worm.radius;

				//Move the ball outside of the worm
				position.x -= 2 * distance * normal.x;
				position.y -= 2 * distance * normal.y;

			}

			//Calculate the distance along the worm at which they collided
			float perpendicularDistance = DotProduct (
				sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y ),
				sf::Vector2f ( position.x - worm.headPosition.x, position.y - worm.headPosition.y )
			) / Modulus (
				sf::Vector2f ( worm.tailPosition.x - worm.headPosition.x, worm.tailPosition.y - worm.headPosition.y )
			);
			float proportion = perpendicularDistance / worm.length;

			//Calculate the velocity of the point of impact on the worm
			sf::Vector2f wormVelocity ( worm.headVelocity.x*(1-proportion) + worm.tailVelocity.x*proportion, worm.headVelocity.y*(1-proportion) + worm.tailVelocity.y*proportion);

			//Calculate relative velocity of the ball
			sf::Vector2f relativeVelocity ( velocity.x - wormVelocity.x, velocity.y - wormVelocity.y );

			float dotProduct = DotProduct ( normal, relativeVelocity );

			//The dot product should always be less than 0, but this will mess up if it isn't
			if ( dotProduct < 0.f ) {
				relativeVelocity.x -= 1.95 * normal.x * dotProduct;
				relativeVelocity.y -= 1.95 * normal.y * dotProduct;
			}

			velocity.x = relativeVelocity.x + wormVelocity.x;
			velocity.y = relativeVelocity.y + wormVelocity.y;

			hitSound.play();
		}
	} else {
		if ( SquareDistance ( position, worm.headPosition ) <= ( radius + worm.radius ) * ( radius + worm.radius ) ) {
			sf::Vector2f normal ( position.x - worm.headPosition.x, position.y - worm.headPosition.y );
			float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
			normal.x /= magnitude;
			normal.y /= magnitude;

			if ( magnitude < radius + worm.radius ) {
				magnitude -= radius + worm.radius;
				position.x -= 2 * magnitude * normal.x;
				position.y -= 2 * magnitude * normal.y;
			}

			float dotProduct = normal.x*velocity.x + normal.y*velocity.y;
			if ( dotProduct < 0 ) {
				velocity.x -= 1.95 * normal.x * ( dotProduct );
				velocity.y -= 1.95 * normal.y * ( dotProduct );
			}

			velocity.x += DotProduct ( worm.headVelocity, normal ) * normal.x;
			velocity.y += DotProduct ( worm.headVelocity, normal ) * normal.y;

			hitSound.play();
		}
	}
}

void MatchState::Worm::update ( double time, Court& court )
{
	//Store old tail position
	sf::Vector2f oldTailPosition = sf::Vector2f ( headPosition.x - length * std::sin ( direction ), headPosition.y - length * std::cos ( direction ) );

	//Friction
	if ( inGround ) {
		sf::Vector2f perpendicular ( tailPosition.y - headPosition.y, headPosition.x - tailPosition.x );
		double modulus = Modulus ( perpendicular );
		perpendicular.x /= modulus;
		perpendicular.y /= modulus;
		float dotProduct = DotProduct( headVelocity, perpendicular );
		headVelocity.x -= dotProduct*perpendicular.x*time*4;
		headVelocity.y -= dotProduct*perpendicular.y*time*4;
	} else {
		sf::Vector2f perpendicular ( tailPosition.y - headPosition.y, headPosition.x - tailPosition.x );
		double modulus = Modulus ( perpendicular );
		perpendicular.x /= modulus;
		perpendicular.y /= modulus;
		float dotProduct = DotProduct( headVelocity, perpendicular );
		headVelocity.x -= dotProduct*perpendicular.x*time*0.5;
		headVelocity.y -= dotProduct*perpendicular.y*time*0.5;
	}

	//Rotation
	if ( controller.left() ) {
		direction -= 4*time;
		if ( !inGround ) {
			direction -= 2*time;
		}
	}
	if ( controller.right() ) {
		direction += 4*time;
		if ( !inGround ) {
			direction += 2*time;
		}
	}

	//Propulsion
	if ( controller.forwards() ) {
		if ( inGround ) {
			headVelocity.x += std::sin ( direction ) * 1200 * time;
			headVelocity.y += std::cos ( direction ) * 1200 * time;
		} else {
			headVelocity.x += std::sin ( direction ) * 200 * time;
			headVelocity.y += std::cos ( direction ) * 200 * time;
		}
	}
	if ( controller.backwards() ) {
		if ( inGround ) {
			headVelocity.x -= std::sin ( direction ) * 1000 * time;
			headVelocity.y -= std::cos ( direction ) * 1000 * time;
		} else {
			headVelocity.x -= std::sin ( direction ) * 160 * time;
			headVelocity.y -= std::cos ( direction ) * 160 * time;
		}
	}

	//Ground collision
	if ( !inGround && ( headPosition.y - radius <= court.groundLevel || tailPosition.y - radius <= court.groundLevel ) ) {
		inGround = true;
		headVelocity.y *= 0.75;
	} else if ( inGround && ! ( headPosition.y - radius <= court.groundLevel || tailPosition.y - radius <= court.groundLevel ) ) {
		inGround = false;
	}

	//Gravity
	if ( !inGround ) {
		headVelocity.y -= 650 * time;
	}

	//Friction
	if ( inGround ) {
		headVelocity.x *= std::pow ( 0.275 - std::cos ( 2 * direction ) * 0.2, time );
		headVelocity.y *= std::pow ( 0.275 + std::cos ( 2 * direction ) * 0.2, time );
	}

	//Collision with the left wall
	if ( headPosition.x + radius >= court.area.left + court.area.width ) {
		headPosition.x = -headPosition.x - 2* radius + 2*(court.area.left + court.area.width);
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( headVelocity.x > 0.f ) {
			headVelocity.x *= -0.2;
			tailVelocity.x *= -0.2;
		}
	}

	if ( tailPosition.x + radius >= court.area.left + court.area.width ) {
		headPosition.x += -2*tailPosition.x - 2*radius + 2*(court.area.left + court.area.width);
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( tailVelocity.x > 0.f ) {
			headVelocity.x -= tailVelocity.x*1.2;
			tailVelocity.x *= -0.2;
		}
	}

	//Collision with the right wall
	if ( headPosition.x - radius <= court.area.left ) {
		headPosition.x = -headPosition.x + 2* radius + 2*court.area.left;
		if ( headVelocity.x < 0.f ) {
			headVelocity.x *= -0.2;
			tailVelocity.x *= -0.2;
		}
	}

	if ( tailPosition.x - radius <= court.area.left ) {
		headPosition.x += -2*tailPosition.x + 2*radius + 2*court.area.left;
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( tailVelocity.x < 0.f ) {
			headVelocity.x -= tailVelocity.x*1.2;
			tailVelocity.x *= -0.2;
		}
	}

	//Collision with the bottom wall
	if ( headPosition.y - radius <= court.area.top - court.area.height ) {
		headPosition.y = -headPosition.y + 2* radius + 2*( court.area.top - court.area.height );
		tailPosition.y = headPosition.y - length * std::cos ( direction );
		if ( headVelocity.y < 0.f ) {
			headVelocity.y *= -0.2;
			tailVelocity.y *= -0.2;
		}
	}

	if ( tailPosition.y - radius <= court.area.top - court.area.height ) {
		headPosition.y += -2*tailPosition.y + 2*radius + 2*( court.area.top - court.area.height );
		tailPosition.y = headPosition.y - length * std::cos ( direction );
		if ( tailVelocity.y < 0.f ) {
			headVelocity.y -= tailVelocity.y*1.2;
			tailVelocity.y *= -0.2;
		}
	}

	//Collision with the top wall
	if ( headPosition.y + radius >= court.area.top ) {
		headPosition.y = -headPosition.y - 2* radius + 2*court.area.top;
		tailPosition.y = headPosition.y - length * std::cos ( direction );
		if ( headVelocity.y > 0.f ) {
			headVelocity.y *= -0.2;
			tailVelocity.y *= -0.2;
		}
	}

	if ( tailPosition.y + radius >= court.area.top ) {
		headPosition.y += -2*tailPosition.y - 2*radius + 2*court.area.top;
		tailPosition.y = headPosition.y - length * std::cos ( direction );
		if ( tailVelocity.y > 0.f ) {
			headVelocity.y -= tailVelocity.y*1.2;
			tailVelocity.y *= -0.2;
		}
	}

	HandleNetCollision ( court );

	//Velocity
	headPosition.x += headVelocity.x * time;
	headPosition.y += headVelocity.y * time;

	//Calculate new tail position
	tailPosition.x = headPosition.x - length * std::sin ( direction );
	tailPosition.y = headPosition.y - length * std::cos ( direction );

	//Calculate tail velocity
	tailVelocity.x = ( tailPosition.x - oldTailPosition.x ) / time;
	tailVelocity.y = ( tailPosition.y - oldTailPosition.y ) / time;

	//Add to trail
	if ( inGround ) {
		trail.add ( headPosition, direction );
	}

	trail.update ( time );

	if ( charged ) {
		frame += time*20;
	} else {
		frame = 0;
	}

}

void MatchState::Worm::HandleNetCollision ( Court& court )
{

	//Collision with the left side of the net
	if (
		headPosition.x + radius >= -court.netRadius &&
		headPosition.x < 0 &&
		headPosition.y <= court.groundLevel + court.netHeight &&
		headPosition.y >= court.groundLevel
	) {
		headPosition.x = -headPosition.x - 2* radius - 2*court.netRadius;
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( headVelocity.x > 0.f ) {
			headVelocity.x *= -0.2;
			tailVelocity.x *= -0.2;
		}
		//netCollisionSound.play();
	}

	if (
		tailPosition.x + radius >= -court.netRadius &&
		tailPosition.x < 0 &&
		tailPosition.y <= court.groundLevel + court.netHeight &&
		tailPosition.y >= court.groundLevel
	) {
		headPosition.x += -2*tailPosition.x - 2*radius - 2*court.netRadius;
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( tailVelocity.x > 0.f ) {
			headVelocity.x -= tailVelocity.x*1.2;
			tailVelocity.x *= -0.2;
		}
		//netCollisionSound.play();
	}

	//Collision with the right side of the net
	if (
		headPosition.x - radius <= court.netRadius &&
		headPosition.x > 0 &&
		headPosition.y <= court.groundLevel + court.netHeight &&
		headPosition.y >= court.groundLevel
	) {
		headPosition.x = -headPosition.x + 2* radius + 2*court.netRadius;
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( headVelocity.x < 0.f ) {
			headVelocity.x *= -0.2;
			tailVelocity.x *= -0.2;
		}
		//netCollisionSound.play();
	}

	if (
		tailPosition.x - radius <= court.netRadius &&
		tailPosition.x > 0 &&
		tailPosition.y <= court.groundLevel + court.netHeight &&
		tailPosition.y >= court.groundLevel
	) {
		headPosition.x += -2*tailPosition.x + 2*radius + 2*court.netRadius;
		tailPosition.x = headPosition.x - length * std::sin ( direction );
		if ( tailVelocity.x < 0.f ) {
			headVelocity.x -= tailVelocity.x*1.2;
			tailVelocity.x *= -0.2;
		}
		//netCollisionSound.play();
	}

	//Collision with the top of the net
	{
		bool behindHead ( false );
		bool behindTail ( false );
		sf::Vector2f netPosition = sf::Vector2f ( 0, court.groundLevel+court.netHeight );
		if (
			DotProduct (
				sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y ),
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			) > 0
		) {
			behindHead = true;
		}

		if (
			DotProduct (
				sf::Vector2f ( netPosition.x - tailPosition.x, netPosition.y - tailPosition.y ),
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			) > 0
		) {
			behindTail = true;
		}

		if ( behindTail ) {
			if ( SquareDistance ( netPosition, tailPosition ) <= ( court.netRadius + radius ) * ( court.netRadius + radius ) ) {
				sf::Vector2f normal ( tailPosition.x - netPosition.x, tailPosition.y - netPosition.y );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				if ( magnitude < court.netRadius + radius ) {
					magnitude -= court.netRadius + radius;
					headPosition.x -= 2 * magnitude * normal.x;
					headPosition.y -= 2 * magnitude * normal.y;
					tailPosition.x -= 2 * magnitude * normal.x;
					tailPosition.y -= 2 * magnitude * normal.y;
				}

				float dotProduct = normal.x*tailVelocity.x + normal.y*tailVelocity.y;

				if ( dotProduct < 0 ) {
					headVelocity.x -= 1.2 * normal.x * ( dotProduct );
					headVelocity.y -= 1.2 * normal.y * ( dotProduct );
					tailVelocity.x -= 1.2 * normal.x * ( dotProduct );
					tailVelocity.y -= 1.2 * normal.y * ( dotProduct );
				}
				//netCollisionSound.play();
			}
		} else if ( behindHead ) {

			//Calculate the perpendicular distance from the worm to the net
			float distance =
			ModCrossProduct (
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y ),
				sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y )
			) / Modulus (
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			);

			//The collision only occurs if this distance is less than their combined radii
			if (
				distance < radius + court.netRadius
			) {

				//Calculate the normalised normal vector of the worm's surfce
				sf::Vector2f normal ( tailPosition.y - headPosition.y, headPosition.x - tailPosition.x );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				//The normal must point out of the net towards the worm
				if ( DotProduct ( normal, sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y ) ) > 0.f ) {
					normal.x *= -1;
					normal.y *= -1;
				}

				if ( distance < radius + court.netRadius ) {

					//Distance is now the overlap between the two
					distance -= radius + court.netRadius;

					//Move the worm outside of the net
					headPosition.x -= 2 * distance * normal.x;
					headPosition.y -= 2 * distance * normal.y;
					tailPosition.x -= 2 * distance * normal.x;
					tailPosition.y -= 2 * distance * normal.y;

				}

				//Calculate the distance along the worm at which they collided
				float perpendicularDistance = DotProduct (
					sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y ),
					sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y )
				) / Modulus (
					sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
				);
				float proportion = perpendicularDistance / length;

				//Calculate the velocity of the point of impact on the worm
				sf::Vector2f wormVelocity ( headVelocity.x*(1-proportion) + tailVelocity.x*proportion, headVelocity.y*(1-proportion) + tailVelocity.y*proportion);

				float dotProduct = DotProduct ( normal, wormVelocity );

				//The dot product should always be less than 0, but this will mess up if it isn't
				if ( dotProduct < 0.f ) {
					wormVelocity.x -= 1.2 * normal.x * dotProduct;
					wormVelocity.y -= 1.2 * normal.y * dotProduct;
				}

				headVelocity.x = wormVelocity.x;
				headVelocity.y = wormVelocity.y;

				//netCollisionSound.play();
			}
		} else {
			if ( SquareDistance ( netPosition, headPosition ) <= ( court.netRadius + radius ) * ( court.netRadius + radius ) ) {
				sf::Vector2f normal ( headPosition.x - netPosition.x, headPosition.y - netPosition.y );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				if ( magnitude < court.netRadius + radius ) {
					magnitude -= court.netRadius + radius;
					headPosition.x -= 2 * magnitude * normal.x;
					headPosition.y -= 2 * magnitude * normal.y;
					tailPosition.x -= 2 * magnitude * normal.x;
					tailPosition.y -= 2 * magnitude * normal.y;
				}

				float dotProduct = normal.x*headVelocity.x + normal.y*headVelocity.y;
				if ( dotProduct < 0 ) {
					headVelocity.x -= 1.2 * normal.x * ( dotProduct );
					headVelocity.y -= 1.2 * normal.y * ( dotProduct );
					tailVelocity.x -= 1.2 * normal.x * ( dotProduct );
					tailVelocity.y -= 1.2 * normal.y * ( dotProduct );
				}

				//netCollisionSound.play();
			}
		}
	}

	//Collision with the top of the net
	{
		bool behindHead ( false );
		bool behindTail ( false );
		sf::Vector2f netPosition = sf::Vector2f ( 0, court.groundLevel );
		if (
			DotProduct (
				sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y ),
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			) > 0
		) {
			behindHead = true;
		}

		if (
			DotProduct (
				sf::Vector2f ( netPosition.x - tailPosition.x, netPosition.y - tailPosition.y ),
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			) > 0
		) {
			behindTail = true;
		}

		if ( behindTail ) {
			if ( SquareDistance ( netPosition, tailPosition ) <= ( court.netRadius + radius ) * ( court.netRadius + radius ) ) {
				sf::Vector2f normal ( tailPosition.x - netPosition.x, tailPosition.y - netPosition.y );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				if ( magnitude < court.netRadius + radius ) {
					magnitude -= court.netRadius + radius;
					headPosition.x -= 2 * magnitude * normal.x;
					headPosition.y -= 2 * magnitude * normal.y;
					tailPosition.x -= 2 * magnitude * normal.x;
					tailPosition.y -= 2 * magnitude * normal.y;
				}

				float dotProduct = normal.x*tailVelocity.x + normal.y*tailVelocity.y;

				if ( dotProduct < 0 ) {
					headVelocity.x -= 1.2 * normal.x * ( dotProduct );
					headVelocity.y -= 1.2 * normal.y * ( dotProduct );
					tailVelocity.x -= 1.2 * normal.x * ( dotProduct );
					tailVelocity.y -= 1.2 * normal.y * ( dotProduct );
				}

				//netCollisionSound.play();
			}
		} else if ( behindHead ) {

			//Calculate the perpendicular distance from the worm to the net
			float distance =
			ModCrossProduct (
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y ),
				sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y )
			) / Modulus (
				sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
			);

			//The collision only occurs if this distance is less than their combined radii
			if (
				distance < radius + court.netRadius
			) {

				//Calculate the normalised normal vector of the worm's surfce
				sf::Vector2f normal ( tailPosition.y - headPosition.y, headPosition.x - tailPosition.x );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				//The normal must point out of the net towards the worm
				if ( DotProduct ( normal, sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y ) ) > 0.f ) {
					normal.x *= -1;
					normal.y *= -1;
				}

				if ( distance < radius + court.netRadius ) {

					//Distance is now the overlap between the two
					distance -= radius + court.netRadius;

					//Move the worm outside of the net
					headPosition.x -= 2 * distance * normal.x;
					headPosition.y -= 2 * distance * normal.y;
					tailPosition.x -= 2 * distance * normal.x;
					tailPosition.y -= 2 * distance * normal.y;

				}

				//Calculate the distance along the worm at which they collided
				float perpendicularDistance = DotProduct (
					sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y ),
					sf::Vector2f ( netPosition.x - headPosition.x, netPosition.y - headPosition.y )
				) / Modulus (
					sf::Vector2f ( tailPosition.x - headPosition.x, tailPosition.y - headPosition.y )
				);
				float proportion = perpendicularDistance / length;

				//Calculate the velocity of the point of impact on the worm
				sf::Vector2f wormVelocity ( headVelocity.x*(1-proportion) + tailVelocity.x*proportion, headVelocity.y*(1-proportion) + tailVelocity.y*proportion);

				float dotProduct = DotProduct ( normal, wormVelocity );

				//The dot product should always be less than 0, but this will mess up if it isn't
				if ( dotProduct < 0.f ) {
					wormVelocity.x -= 1.2 * normal.x * dotProduct;
					wormVelocity.y -= 1.2 * normal.y * dotProduct;
				}

				headVelocity.x = wormVelocity.x;
				headVelocity.y = wormVelocity.y;

				//netCollisionSound.play();
			}
		} else {
			if ( SquareDistance ( netPosition, headPosition ) <= ( court.netRadius + radius ) * ( court.netRadius + radius ) ) {
				sf::Vector2f normal ( headPosition.x - netPosition.x, headPosition.y - netPosition.y );
				float magnitude = std::sqrt ( normal.x*normal.x + normal.y*normal.y );
				normal.x /= magnitude;
				normal.y /= magnitude;

				if ( magnitude < court.netRadius + radius ) {
					magnitude -= court.netRadius + radius;
					headPosition.x -= 2 * magnitude * normal.x;
					headPosition.y -= 2 * magnitude * normal.y;
					tailPosition.x -= 2 * magnitude * normal.x;
					tailPosition.y -= 2 * magnitude * normal.y;
				}

				float dotProduct = normal.x*headVelocity.x + normal.y*headVelocity.y;
				if ( dotProduct < 0 ) {
					headVelocity.x -= 1.2 * normal.x * ( dotProduct );
					headVelocity.y -= 1.2 * normal.y * ( dotProduct );
					tailVelocity.x -= 1.2 * normal.x * ( dotProduct );
					tailVelocity.y -= 1.2 * normal.y * ( dotProduct );
				}

				//netCollisionSound.play();
			}
		}
	}
}
