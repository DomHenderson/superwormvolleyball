#include <chrono>
#include <cmath>
#include <iostream>
#include <memory>

#include <SFML/Window/Mouse.hpp>

#include <SFML/Graphics/RenderWindow.hpp>

#include "aspectRatio.hpp"
#include "gameState.hpp"
#include "stateStack.hpp"
#include "titleScreenState.hpp"

int main()
{
	//Set up window
	sf::ContextSettings settings;
	settings.antialiasingLevel = 4;
	sf::RenderWindow window ( sf::VideoMode ( 1200, 750 ), "SuperWorm Volleyball", sf::Style::Default, settings );
	sf::View view ( {960, 540}, {1920, 1080} );
	window.setView ( view );
	SetAspectRatio( window, 16, 9 );

	//Create GameState Stack
	StateStack stateStack;

	//Load in TitleScreenState
	stateStack.push ( std::make_unique<TitleScreenState> ( stateStack, window ) );
	stateStack.handleTransitions();

	//Frame rate measuring
	unsigned frameCounter ( 0 );

	std::chrono::steady_clock::time_point startTime = std::chrono::steady_clock::now();

	std::chrono::steady_clock::time_point t1 = std::chrono::steady_clock::now();
	std::chrono::steady_clock::time_point t2;
	std::chrono::duration<double> time_span;

	//Main game loop
	while ( true ) {
		stateStack.handleEvents();
		if ( !window.isOpen() ) {
			break;
		}
		t2 = std::chrono::steady_clock::now();
		time_span = std::chrono::duration_cast<std::chrono::duration<double>>(t2-t1);
		t1 = t2;
		stateStack.update( time_span.count() );
		stateStack.draw();
		stateStack.handleTransitions();
		++frameCounter;
	}
	stateStack.end();

	//More frame rate measuring code
	std::chrono::steady_clock::time_point endTime = std::chrono::steady_clock::now();
	std::chrono::duration<double> runTime = std::chrono::duration_cast<std::chrono::duration<double>>(endTime-startTime);
	std::cout<<"It took "<<runTime.count()<<" seconds to display "<<frameCounter<<" frames."<<std::endl;
	std::cout<<"This is a framerate of "<<frameCounter / runTime.count()<<" fps"<<std::endl;

	return 0;
}
