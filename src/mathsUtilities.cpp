#include <cmath>

#include <SFML/System/Vector2.hpp>

#include "mathsUtilities.hpp"

float DotProduct ( sf::Vector2f a, sf::Vector2f b )
{
	return a.x*b.x + a.y*b.y;
}

float SquareDistance ( sf::Vector2f a, sf::Vector2f b )
{
	return (a.x-b.x)*(a.x-b.x) + (a.y-b.y)*(a.y-b.y);
}

float ModCrossProduct ( sf::Vector2f a, sf::Vector2f b )
{
	return std::abs ( a.x * b.y - b.x * a.y );
}

float Modulus ( sf::Vector2f a )
{
	return std::sqrt ( a.x*a.x + a.y*a.y );
}
