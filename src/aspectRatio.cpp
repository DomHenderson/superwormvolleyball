#include "aspectRatio.hpp"

void SetAspectRatio ( sf::RenderWindow& window, unsigned x, unsigned y )
{
	//Current ratio of the window
	double windowRatio = static_cast<double>(window.getSize().y)/window.getSize().x;
	//Desired ratio of the viewport
	double viewRatio = static_cast<double>(y)/x;

	double newHeight;
	double newWidth;

	if ( viewRatio > windowRatio ) {
		//Vertical bars
		newHeight = 1.0;
		newWidth = windowRatio/viewRatio;
	} else {
		//Horizontal bars
		newWidth = 1.0;
		newHeight = viewRatio/windowRatio;
	}

	sf::View v = window.getView();
	v.setViewport(sf::FloatRect(
		(1.0-newWidth)/2,   //Left
		(1.0-newHeight)/2,  //Top
		newWidth,           //Width
		newHeight           //Height
	));

	window.setView(v);
}
