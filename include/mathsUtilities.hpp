#ifndef MATHSUTILITIES_HPP_INCLUDED
#define MATHSUTILITIES_HPP_INCLUDED

#include <SFML/System/Vector2.hpp>

constexpr long double Pi = 3.141592653589793238462643383279503;

float DotProduct ( sf::Vector2f a, sf::Vector2f b );

float SquareDistance ( sf::Vector2f a, sf::Vector2f b );

float ModCrossProduct ( sf::Vector2f a, sf::Vector2f b );

float Modulus ( sf::Vector2f a );

#endif // MATHSUTILITIES_HPP_INCLUDED
