#ifndef TRAIL_HPP_INCLUDED
#define TRAIL_HPP_INCLUDED

#include <deque>
#include <utility>

#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Vertex.hpp>
#include <SFML/Graphics/VertexArray.hpp>

//Creates the illusion that the worms are digging through the ground
class WormTrail : public sf::Drawable {
public:
	WormTrail ( sf::Color colour, float radius, float length );

	//Add a new position to the front of the trail
	void add ( sf::Vector2f position, float angle );

	//Draw the trail
	void draw ( sf::RenderTarget& target, sf::RenderStates states ) const override;

	//Update the fade out animation
	void update ( double time );

	//Reset the trail
	void clear();
private:

	//Deque used because insertions happen at the front and deletions at the back
	std::deque<std::pair<sf::VertexArray,double>> trails;
	sf::Vector2f lastPos;
	sf::Color colour;

	float lastAngle;
	float radius;
	float length;

	//How long any particular stamp lasts
	const double duration = 0.4;
};

#endif // TRAIL_HPP_INCLUDED
