#ifndef CONNECTIONMENUSTATE_HPP_INCLUDED
#define CONNECTIONMENUSTATE_HPP_INCLUDED

#include <atomic>
#include <memory>
#include <stack>
#include <thread>

#include <SFML/Network.hpp>

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "gameState.hpp"
#include "stateStack.hpp"

class ConnectionMenuState : public GameState {
public:
	ConnectionMenuState ( StateStack& stateStack, sf::RenderWindow& window );

	virtual void draw() override;
	virtual void handleEvents() override;
	virtual void update( double time ) override;

private:

	enum MenuState {
		BaseMenu,
		ServerMenu,
		ClientMenu
	};

	MenuState menuState;

	StateStack& stateStack;

	sf::RenderWindow& window;

	sf::Font menuFont;

	sf::Text localText;
	sf::Text hostText;
	sf::Text joinText;
	sf::Text backText;
	sf::Text ipText;
	sf::Text waitingText;
	sf::Text connectingText;

	sf::Texture backgroundTexture;

	sf::Sprite backgroundSprite;

	sf::TcpSocket socket;
	sf::TcpListener listener;

	std::thread connectingThread;
	std::atomic<bool> connectedToOpponent;
};

#endif // CONNECTIONMENUSTATE_HPP_INCLUDED
