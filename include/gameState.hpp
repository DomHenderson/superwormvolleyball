#ifndef GAMESTATE_HPP_INCLUDED
#define GAMESTATE_HPP_INCLUDED

class GameState {
public:
	virtual void draw() =0;
	virtual void handleEvents() =0;
	virtual void update ( double time ) =0;
};

#endif // GAMESTATE_HPP_INCLUDED
