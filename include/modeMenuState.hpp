#ifndef MODEMENUSTATE_HPP_INCLUDED
#define MODEMENUSTATE_HPP_INCLUDED

#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "gameState.hpp"
#include "stateStack.hpp"

class ModeMenuState : public GameState {
public:
	ModeMenuState ( StateStack& stateStack, sf::RenderWindow& window );

	virtual void draw() override;
	virtual void handleEvents() override;
	virtual void update( double time ) override;
private:
	StateStack& stateStack;

	sf::RenderWindow& window;

	sf::Font menuFont;
	sf::Font titleFont;

	sf::Text titleText;
	sf::Text regularText;
	sf::Text superText;
	sf::Text verySuperText;
	sf::Text backText;

	sf::Texture backgroundTexture;
	sf::Sprite backgroundSprite;
};

#endif // MODEMENUSTATE_HPP_INCLUDED
