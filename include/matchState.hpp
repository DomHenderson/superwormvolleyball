#ifndef MATCHSTATE_HPP_INCLUDED
#define MATCHSTATE_HPP_INCLUDED

#include <functional>
#include <memory>
#include <optional>
#include <random>

#include <SFML/System/Vector2.hpp>

#include <SFML/Graphics/CircleShape.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Rect.hpp>
#include <SFML/Graphics/RectangleShape.hpp>
#include <SFML/Graphics/RenderTexture.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Shader.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>

#include <SFML/Audio/Music.hpp>
#include <SFML/Audio/Sound.hpp>
#include <SFML/Audio/SoundBuffer.hpp>

#include "gameState.hpp"
#include "stateStack.hpp"
#include "trail.hpp"

class MatchState : public GameState {
public:
	//Construct a multiplayer match
	MatchState ( StateStack& stateStack, sf::RenderWindow& window, float chargeRate = 0.f );

	void draw() override;
	void handleEvents() override;
	void update ( double time ) override;

private:

	StateStack& stateStack;

	float runningTime;

	struct ScoreBoard {
		ScoreBoard( std::function<void(void)> newPoint, sf::Color leftColour, sf::Color rightColour );

		void addLeftBounce();
		void addRightBounce();

		std::function<void(void)> newPoint;

		bool rightServe;

		unsigned leftPoints;
		unsigned leftBounces;
		unsigned rightPoints;
		unsigned rightBounces;

		sf::CircleShape pointCircles[9];
		sf::CircleShape bounceCircles[5];

		sf::Font victoryFont;
		sf::Text victoryText;

		sf::Color leftColour;
		sf::Color rightColour;
	};

	struct Court {
		Court ( sf::FloatRect area, float groundLevel, float grassDepth, float netHeight, float netRadius );

		sf::FloatRect area;
		float groundLevel;
		float netHeight;
		float netRadius;

		sf::RectangleShape skyRect;
		sf::RectangleShape grassRect;
		sf::RectangleShape groundRect;
		sf::RectangleShape netRect;
		sf::CircleShape netTopCircle;
		sf::CircleShape netBottomCircle;
	};

	struct Worm {
	public:
		enum class ControlScheme  {
			Wasd,
			ArrowKeys
		};

		struct Controller {
			std::function<bool(void)> forwards;
			std::function<bool(void)> left;
			std::function<bool(void)> right;
			std::function<bool(void)> backwards;
		};

		Worm ( sf::Vector2f position, ControlScheme controlScheme, sf::Color bodyColour, sf::Color eyeColour );
		void update ( double time, Court& court );
		void PrepareTexture();
	private:
		void HandleNetCollision ( Court& court );
	public:
		float radius;
		float length;
		float direction;
		bool inGround;
		sf::Vector2f headPosition;
		sf::Vector2f headVelocity;
		sf::Vector2f tailPosition;
		sf::Vector2f tailVelocity;

		static Controller GetController ( ControlScheme controls );

		Controller controller;

		sf::Sprite sprite;
		sf::Sprite sheetSprite;
		sf::Texture textureSheet;
		sf::RenderTexture texture;

		double frame;
		bool charged = false;

		sf::Color colour;

		//sf::Sound netCollisionSound;
		//sf::SoundBuffer netCollisionSoundBuffer;

		WormTrail trail;
	};

	struct Ball {
	public:
		void update ( double time, Court& court, ScoreBoard& scoreBoard, Worm& leftWorm, Worm& rightWorm );
	private:
		void HandleWormCollision ( Worm& worm );
	public:
		sf::Vector2f position;
		sf::Vector2f velocity;
		float radius;
		sf::CircleShape circle;

		float hitVolume;
		sf::Sound hitSound;
		sf::SoundBuffer hitSoundBuffer;
	};

	struct PowerUI {
	public:
		enum class Power {
			None,
			Push,
			Pull,
			Freeze
		};

		PowerUI ( Ball& b, Worm& left, Worm& right, ScoreBoard& score, float chargeSpeed );
		void update ( double time );
		void useLeftPower() { useLeft = true; }
		void useRightPower() { useRight = true; }

		float getChargeSpeed() { return speed; }
	private:
		Ball& ball;
		Worm& leftWorm;
		Worm& rightWorm;
		ScoreBoard& score;

		float speed;
		float leftCharge;
		float rightCharge;

		Power leftPower = Power::None;
		Power rightPower = Power::None;

		bool useLeft;
		bool useRight;
		float leftTime;
		float rightTime;

		float range;
		float strength;
		float freezeTime;

		std::default_random_engine generator;
		std::uniform_int_distribution<int> distribution;

		float leftDirection;
		float rightDirection;
	public:
		sf::RectangleShape leftBar;
		sf::RectangleShape leftOutline;
		sf::RectangleShape rightBar;
		sf::RectangleShape rightOutline;
	};

	ScoreBoard scoreBoard;

	Court court;

	Ball ball;

	Worm leftWorm;
	Worm rightWorm;

	std::optional<PowerUI> powerUI;

	sf::RenderTexture renderTexture;
	sf::RenderWindow& window;

	std::unique_ptr<sf::Music> groundTrack;

	sf::Font menuFont;

	sf::Text againText;
	sf::Text backText;
};

#endif // MATCHSTATE_HPP_INCLUDED
