#ifndef STATESTACK_HPP_INCLUDED
#define STATESTACK_HPP_INCLUDED

#include <memory>
#include <stack>

#include "gameState.hpp"

class StateStack {
public:
	StateStack();

	void handleEvents();
	void update ( double time );
	void draw();
	void handleTransitions();

	void push ( std::unique_ptr<GameState>&& state );
	void pop ();
	void end();

private:
	std::stack<std::unique_ptr<GameState>> stack;

	bool popFlag;
	std::unique_ptr<GameState> next;
};

#endif // STATESTACK_HPP_INCLUDED
