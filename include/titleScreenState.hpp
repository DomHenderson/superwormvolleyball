#ifndef TITLESCREENSTATE_HPP_INCLUDED
#define TITLESCREENSTATE_HPP_INCLUDED

#include <SFML/Graphics/RenderWindow.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <SFML/Graphics/Text.hpp>
#include <SFML/Graphics/Texture.hpp>

#include "gameState.hpp"
#include "stateStack.hpp"

class TitleScreenState : public GameState {
public:
	TitleScreenState ( StateStack& stateStack, sf::RenderWindow& window );

	virtual void draw() override;
	virtual void handleEvents() override;
	virtual void update( double time ) override;

private:

	StateStack& stateStack;

	sf::RenderWindow& window;

	sf::Font startFont;
	sf::Font titleFont;

	sf::Text startText;
	sf::Text titleText;

	sf::Texture backgroundTexture;

	sf::Sprite backgroundSprite;
};

#endif // TITLESCREENSTATE_HPP_INCLUDED
