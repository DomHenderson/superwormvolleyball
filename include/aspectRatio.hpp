#ifndef ASPECTRATIO_HPP_INCLUDED
#define ASPECTRATIO_HPP_INCLUDED

#include <SFML/Graphics/RenderWindow.hpp>

void SetAspectRatio ( sf::RenderWindow& window, unsigned x, unsigned y );

#endif // ASPECTRATIO_HPP_INCLUDED
